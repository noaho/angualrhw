// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyA2jayH8aKAJRdCE_6v3pjBGmFvVWnDaOY",
    authDomain: "messages-3b0fe.firebaseapp.com",
    databaseURL: "https://messages-3b0fe.firebaseio.com",
    projectId: "messages-3b0fe",
    storageBucket: "messages-3b0fe.appspot.com",
    messagingSenderId: "32465666642"
  }
};
