import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MessagesService } from './../messages.service';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {

  @Output() addMessage:EventEmitter<any> = new  EventEmitter<any>();
  @Output() addMessagePs:EventEmitter<any> = new  EventEmitter<any>();
  

  service:MessagesService;

msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl(),

  });
  constructor(private route:ActivatedRoute , service:MessagesService) { 
    this.service = service;
  }

  sendData(){
    this.addMessage.emit(this.msgform.value.message);
    console.log(this.msgform.value);
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putMessage(this.msgform.value, id).subscribe(
      response =>{
        console.log(response.json())
        this.addMessagePs.emit();
      }
    );
  })
  }
message;

  ngOnInit() {
      this.route.paramMap.subscribe(params=>{
      
      let id = params.get('id');
      console.log(id);
      this.service.getMessage(id).subscribe(response=>{
        this.message = response.json();
        console.log(this.message);
      })
    })
  }

}
