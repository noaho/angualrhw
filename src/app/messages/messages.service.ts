import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http , Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {AngularFireDatabase} from 'angularfire2/database';
import 'rxjs/Rx';
@Injectable()
export class MessagesService {
  http:Http;
getMessages(){
  //return ['Message1','Message2', 'Message3','Message4'];
  //get messages from the SLIM REST API 
   return this.http.get(environment.url +'messages');
 }
  /*getMessages(){
 //return ['Message1','Message2', 'Message3','Message4'];
 //get messages from the SLIM REST API 
 //let token = localStorage.getItem('token');
 /*let options = {
   headers: new Headers({
    // 'Authorization': 'Bearer '+token
   })
 }
  return this.http.get('http://localhost/angular/slim/messages' )
}*/


getMessagesFire(){
  return this.db.list('/messages').valueChanges();

}
getMessage(id){
   return this.http.get(environment.url +'messages/'+id);
}
postMessage(data){
  let token = localStorage.getItem('token');
  let options = {
    headers:new Headers({
      'content-type':'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+token
    })
  }
  let params = new HttpParams().append('message', data.message);
  return this.http.post(environment.url +'messages',
params.toString(), options);

}
deleteMessage(key){
  return this.http.delete(environment.url +'messages/' + key);
}

/*login(credentials){
   let options = {
    headers:new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
   let params = new HttpParams().append('user', credentials.user).append('password',credentials.password);
   return this.http.post(environment.url +'messages',params.toString(),options).map(response=>
    {let token = response.json().token;
  if (token) localStorage.setItem('token', token);
})


}*/
putMessage(data,key){
  let options = {
    headers:new Headers({
      'content-type':'application/x-www-form-urlencoded'
    })
  }
  let params = new HttpParams().append('message', data.message);
  return this.http.put(environment.url +'messages/'  + key, params.toString(), options);
}

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }
    
}
