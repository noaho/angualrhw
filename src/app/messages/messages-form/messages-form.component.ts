import { element } from 'protractor';
import { MessagesService } from './../messages.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
@Component({
  selector: 'messagesForm',
  templateUrl: './messages-form.component.html',
  styleUrls: ['./messages-form.component.css']
})
export class MessagesFormComponent implements OnInit {

  @Output() addMessage:EventEmitter<any> = new  EventEmitter<any>();
  @Output() addMessagePs:EventEmitter<any> = new  EventEmitter<any>();

  service:MessagesService;
  msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl(),

  });
  sendData(){
    this.addMessage.emit(this.msgform.value.message);
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response =>{
        console.log(response.json())
        this.addMessagePs.emit();
      }
    )
  }
  constructor(service:MessagesService) {
    this.service = service;
   }

  ngOnInit() {
  }

}
