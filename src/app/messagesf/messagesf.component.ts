import { MessagesService } from './../messages/messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messagesf',
  templateUrl: './messagesf.component.html',
  styleUrls: ['./messagesf.component.css']
})
export class MessagesfComponent implements OnInit {
messages;
  constructor(private service:MessagesService) { }

  ngOnInit() {
    this.service.getMessagesFire().subscribe(respones =>
    {
      console.log(respones);
      this.messages = respones;
    })
  }

}
