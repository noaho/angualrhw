import { Injectable } from '@angular/core';
import {Http , Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import 'rxjs/Rx';
@Injectable()
export class UsersService {
  http:Http;

  getUsers(){
 //return ['Message1','Message2', 'Message3','Message4'];
 //get messages from the SLIM REST API 
  //return this.http.get('http://localhost/angular/slim/users');
  let token = localStorage.getItem('token');
       let options = {
          headers: new Headers({
            'Authorization':'Bearer '+token
          })
}
         return this.http.get('http://localhost/test/slim/users' , options);
  }
  constructor(http:Http) { 
    this.http = http;
  }

  postUsers(data){
  let token = localStorage.getItem('token');
  let options = {
    headers:new Headers({
      'content-type':'application/x-www-form-urlencoded',
      'Authorization':'Bearer '+token
    })
  }
  let params = new HttpParams().append('user', data.user);
  return this.http.post('http://localhost/angular/slim/users',
params.toString(), options);

}
    
}
